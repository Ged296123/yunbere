**I have moved this website to my [homeserver](https://yunbere.hayalmasal.org), so it is no more maintained. The website is being kept here to provide a sample for my tutorial post on creating a "free" blog on gitlab.**  
***Kendi [web siteme](https://yunbere.hayalmasal.org) taşıdığım için, bu alan artık kullanılmamaktır. Yazılarımdan birine örnek olması için blogumun dondurulmuş versiyonunu bu alanda tutabildiğim kadar tutmaya çalışacağım.*** 

This project is a personal blog site created with a Hugo template using the latest docker image found in GitLab.

The project website can be viewed from [here](https://ged296123.gitlab.io/yunbere/).

You may access the Hugo website from [here](https://gohugo.io/).  
You may fork the mentioned Hugo template from [here](https://gitlab.com/pages/hugo).

The existing theme of the project is Hugo PaperMod. You may download the theme from [here](https://github.com/adityatelange/hugo-PaperMod).

This website with its content is "Free."

Hugo and PaperMod are both licenced under the MIT License. All content material produced by the author of this site ("Ged") are licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

This website believes in supporting the internet and protecting its users from **bloated** websites. For this reason this website is a proud "GREEN" member of the [512kb Club](https://512kb.club/).   

