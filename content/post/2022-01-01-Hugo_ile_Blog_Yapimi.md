---
title: Bu Blog Gibi Bir Site de Siz Kurmak İster misiniz?
date: 2022-01-01
tags: ["atölye","izleniyoruz"]
draft: false
showtoc: true
---

Cevabınız evetse, bu yazı size yardımcı olabilir...

*Bu blog, olduğu gibi https://yunbere.hayalmasal.org adresine taşınmıştır. Blog'un güncel sayfalarına belirtilen adresten ulaşabilirsiniz.  

Sadece bu post, kendi blogunu kurmak isteyecek arkadaşlar için örnek olması için bırakılmıştır.*

# Neden
Sosyal medyaya tepkimi ne kadar dile getirsem az. Bir curcuna ve keşmekeş içerisinde çoğu niteliksiz yazı ve düşüncenin olduğu, insanların özgürlük kisfesi altında kendilerini ifşa ettikleri, sürekli izlendikleri ve bunun farkında bile olmadıkları bir düzlem oldu sosyal medya. 

Halbuki zamanı biraz geri sararsak, şöyle 90'lar belki 2000'lerin başına dönersek, yazmak ve paylaşmak isteyenler için özgür bir platform sunuyordu internet. Gazete ve dergide yazmayan / yazamayanlar, internet sayesinde kendi gazetelerini yayınlayabiliyordu bloglarla. Tanıdığınız, denk geldiğiniz, sevdiğiniz insanların bu bloglarına ara ara girer (veya biraz daha yatkınsak RSS üzerinden kaydolur) yazdıklarını, çizdiklerini takip ederdik. O zamanlar internet daha masum ve insanı odağından kaçırmayan bir mecraydı.

Yıllarla aradan sonra, ben de yeniden ufak bir blog tutmaya giriştim. Daha çok yazmak ve paylaşmış olmak olsa da amacım, özellikle bu yazımı birileri okusun ve kendi blogunu kursun çok isterim... Bir dönem bilgim olmadığı için özgür olmayan platformlarda blog tuttum. Ancak sonraları (hiçbir zaman geç değil) işi buraya aktarabildim. 

Malum her özgür yazılım düşüncesinin ardında topluluğa geri verme var; ancak böyle, paylaşarak büyüyebiliyoruz. Ben de bu siteyi oluştururken edindiğim bilgileri derleyip, Türkçe olarak kaleme almaya çalıştım. Kendi blogunu oluşturmak isteyen arkadaşlarımla paylaşmak istedim.    

# Ne Kullandım
Nasıl oluşturduğuma geçmeden önce, hangi araçları, mecraları kullandığım kısaca özetlemek isterim. 

Sitenin oluşturulmasında ["Hugo"](https://gohugo.io/) kullandım. Bunları tek tek paylaşıyorum; çoğunun nedenine girmeyeceğim, yazının amacını aşacağı gibi kapsamını da çok genişleteceğini düşünüyorum.

Hugo'yu, kolaylıkla web sitenizi oluşturup yayına alabilen, makale ve yazılarınızı basit bir text editor (daktilo) ile yazabileceğiniz arayüz gibi düşünebilirsiniz. Hugo kullanırken HTML gibi "web dillerini" bilmenize gerek yok. Hatta bu yazıda göreceksiniz, Hugo'yu bilgisayarınıza kurmanıza bile gerek kalmayacak.

Akabinde sitenizin görselliğini elde etmek için bir Hugo teması seçebilirsiniz. Bu gördüğünüz sitenin teması [PaperMod](https://adityatelange.github.io/hugo-PaperMod/). Binlerce tema var, araştırıp başkalarını da seçebilirsiniz. Hepimizin sitesi aynı görsellikle olmasa bence daha iyi olur.

Postlarınızı yazmak için herhangi bir text editor kullanabilirsiniz. Yazdıklarınızı biçimlendirerek yazmak istiyorsanız, çok basit "Markdown" şablonları var, onları da ayrıca bir link ile paylaşcağım. 

Son olarak, siteyi yayınlayacağınız bir server gerekiyor. Bu noktada [Gitlab](https://about.gitlab.com/) imdada yetişti. Gitlab, sitenizi ücretsiz yayınlayabiliyor. Hatta isterseniz ve bir alan adınız varsa, sitenizi o alan adı üzerinden de yayınlayabilirsiniz.

Gördüğünüz siteyi özetlersem;

- Sitenin oluşturulmasında Hugo kullandım,
- Sitenin görselliğini PaperMod temasıyla verdim, 
- Postlarımı (yazılarımı) Apostrophe ile yazıyorum, isterseniz Vim, Gedit, Nano gibi birçok alternatif text veya Markdown editoru kullanabilirsiniz. 
- Siteyi Gitlab sunucuları üzerinden yayınlıyorum.

# Gitlab'de Hesap Oluşturmak
Sitemizi yaratırken bilgisayarımıza hiçbir uygulama yüklemek durumunda değiliz. Bu yöndeki tüm ihtiyacımızı Gitlab bizim için karşılayacak. Öncelikle Gitlab üzerinde bir hesap oluşturmanız gerekecek.  

Tarayıcınızdan [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) adresine girip, kullanıcı bilgileriniz doldurun ve herhangi bir siteye kayıt olur gibi kaydolun.

Kaydolurken kullandığınız e-posta hesabına bir onay iletisi gelecek. Bu ileti içerisinde ilgili linke tıklayarak hesabınızı onaylayın.

Yeni açılan sayfada kullanıcı adınız ve belirlediğiniz şifreyle giriş yapcaksınız. İlk girişinizde Gitlab size birkaç profil sorusu soracak; bunları ilk ikisiniz istediğiniz gibi cevaplayabilirsiniz. *"Who will be using Gitlab"* (Gitlab'i kim kullanacak) sorusuna *"Just me"* (sadece ben) demeniz, *"What would you like to do?"* (Ne yapmak istersiniz?) sorusuna *"Create a new project"* (Yeni bir proje oluştur) seçeneklerini işaretlemeniz gerekir. 

Sonraki pencerede *"Create"* (Yarat) sekmesi altında sitenize bir "Grup adı" ve "Proje adı" belirlemeniz istenecek. Bunları da uygun şekilde doldurabilir ve ekranın altındaki *"Create Project"* tuşuna basabilirsiniz. Proje adının, sitenizin ismi veya bu isme yakın olması konusunda özen gösterirseniz iyi olur. Web sitenizin adresi, burada seçeceğiniz Proje adı'na göre oluşturulacaktır.

Onay ekranından sonra, isterseniz projenize başka kişileri de davet edebileceğiniz bir e-posta kayıt ekranı açılacak. Buna şimdilik *"Cancel"* diyerek geçebiliriz. 

Yeni Gitlab hesabınızı iyi günlerde kullanmanızı dilerim.

# Hugo Imajı Forklamak
Sitemizi Gitlab üzerinden sunacağımız için, hazır bir container image'dan fork alabileceğiz. Bunun ne demek olduğunu anlamdıysanız hiç sorun değil. Bilmeniz gereken, yazının en başında da bahsettiğim gibi bilgisayarımıza bir uygulama kurmadan devam edebilyor olacağımız. 

Uygulamayı, Gitlab'de halihazırda bulunan bir altyapıyı kullanarak, yine Gitlab üzerinden çalıştıracağız. Bu yazdığım da birşey ifade etmiyorsa takılmayın, aşağıdaki komutları sırasıyla takip edebilirsiniz. 

[https://gitlab.com/pages/hugo/-/forks/new](https://gitlab.com/pages/hugo/-/forks/new) linkini kullanarak, kendinize yeni bir Hugo site "fork"[^1] edin. Projenizin ismini, *"Project Name"* başlığı altında site isminiz olarak belirleyebilirsiniz. Akabinde, *"Project URL*" ve "*Project Slug*" kısımlarından web sitenizin adresini tanımlayabilirsiniz. 

[^1]: "Fork" tabiri, Git jargonunda kullanılan bir terim. Sizden önce birisnin uygulama geliştirmek için olşturduğu repertuvarı (veya uygulama dizinini) kopyalayarak bir nüsha kendimize alıyoruz. Türkçe karşılığı "çatal" anlamına gelen "fork," aslında uygulamanın gelişiminde bir çatal / yol ayrımına işaret ediyor.  

*"Project Description"* (Proje açıklaması) kısmına kısa bir açıklama yazabilirsiniz. Eğer ilk başta ne yazacağınızı bilmiyorsanız dert etmeyin, bu alan sonradan da düzenlenebilir. 

*"Visibility Level"* (Görünürlük düzeyi) en başta *Private* (Gizli) olarak seçilmek durumunda. Bu ayarı ilerleyen aşamalarda değiştireceğiz. 

Seçimleri ve alanları tamamladıktan sonra *"Fork Project"* tuşuna basabilirsiniz! Tebrikler... Artık bir Hugo siteniz var (henüz ayarları yapılmadığı için çalışmıyor, ancak bu çok sürmeyecek!).

# İlk Ayarlar
Siteyi "fork"ladıktan (yani bir kopyasını kendi hesabımıza kopyaladıktan) sonra, sırasıyla aşağıdaki işlemleri yapmanız gerekecek:

1. *Menu > CI/CD > Pipelines* sayfasında *"Run Pipeline"* tuşuna basın. Herhangi bir ayar değişikliği yapmadan tekrar *"Run Pipeline"* tuşuyla devam edin.

*Burada hesabınızın doğrulanması için Gitlab sizden kredi kartı bilgisi isteyecek. Bir sanal kart numarasıyla giriş yapmanızı öneririm. Sizden herhangi bir ücret tahsil edilmeyecek.*

Bir bardak çay alarak bekleyin. Ilk pipeline çalışması biraz zaman alabilir... Herşeyin doğrulandığını ve uygun ilerlediğini, bir hata olmadığını teyit edin.

2. *Menu > Settings > General* sayfasında Proje adını, açıklamasını vb. bilgileri değiştirebilirsiniz. En alttaki *"Advanced"* (Gelişmiş) sekmesinde, *"Change path"* başlığı atında sitenizin repo adresinizi kontrol edebilir / değiştirebilirsiniz.
Benim sitemin adresi `https://gitlab.com/Ged296123/yunbere`

Akabinde, aynı sayfanın biraz aşağısında *"Remove fork relationship"* tuşuna basarak, kopyasını aldığınız ana siteyle bağınızı koparın.

3. Yine *Menu > Settings > General* sayfasında *"Visibility, project features, permissions"* (Görünürlük, proje özellikleri, izinler) sekmesini genişleterek, ***Project Visibility: Public (Users can Request Access)*** (Proje Görünürlüğü: Herkese açık - kullanıcılar erişim talep edebilir) ve diğer tüm ayarları, *"Everyone with Access"* (Yetkisi olan herkesin) erişimine açabilirsiniz. 

Yapacağınız değişiklikler derhal geçerli olacaktır. Bu sebeple, Gitlab'den çıkış yapıp (logout) sitenizin adresini denemenizi öneririm (ör: [https://gitlab.com/Ged296123/yunbere](https://gitlab.com/Ged296123/yunbere)). 

404 sayfa hatası (veya başka bir hata) almadıysanız ve Gitlab'de projenizin sayfası karşınıza çıkıyorsa, herşey yolunda demektir.

# Git ile Bilgisayarınızda Klasör Düzeninizi Oluşturmak
Altyapı çalışmaları tamamlandığına göre, süreci şimdi biraz da yerel PC'mizde devam ettireceğiz. Sisteminizde "Git" yüklü olup olmadığına bakın, yüklü değilse yükleyin.[^2]

[^2]: Örneğin Debian, Mint, Ubuntu, PopOS, ElementaryOS kullanıyorsanız; `sudo apt install git` şeklinde. Kendi reponuza / işletim sisteminize göre uygulamayı bulacağınıza eminim.

Yerel PC'nizde, siteniz için bir klasör oluşturun. [^3] Yazılarınızı (ve onlara bağlı olan görseller ve dosyaları) bu klasör içinde yaratıp, sonradan Gitlab'e yükleyeceğiz.  

[^3]: Git proje adınızla yeni bir klasör oluşturacak. Bu sebeple, aynı isimle iç içe iki klasör olmasını istemiyorsanız, oluşturmak istediğiniz klasörün bir üst yolunda kalabilirsiniz.  

Git yüklendikten sonra, oluşturduğunuz yeni klasör yoluna gidin ve *proje_adresiniz*'i uygun şekilde (bir önceki safhada Gitlab içinde belirlenen path'le uyumlu olacak şekilde) düzenleyerek, 

`git clone https://gitlab.com/proje_adresiniz` 

veya örneğin `git clone https://gitlab.com/Ged296123/yunbere`

komutunu girin. Komut çalıştığında sizden Gitlab kullanıcı adı ve parolası istenecek, bunları sırasıyla girebilirsiniz. Herhangi bir hata almadıysanız, Gitlab'de oluşturduğunuz düzen yerel PC'nize kopyalanmıştır; güle güle kullanın!

# Kullanacağımız Git Komutları
Git komutlarıyla, PC'mizi ve buluttaki Gitlab repomuzu senkron tutabileceğiz. Henüz hiçbir blog postumuz yok. O sebeple, nasıl yükleme yapılacağını bilmeniz bu aşamada gerekli görünmese de, ilerki bölümleri okudukça buraya dönebilirsiniz.  

Yazımı basit tutmak için Git öğretmek niyetinde değilim (kendim de ihtiyacım kadarını biliyorum). Yukarıda kullandığımız "clone" haricinde işinizi görmesi için sizinle ezberleyeceğiniz 3 basit komut paylaşacağım.

Bu üç komutla tüm işinizi görecekseniz. Bir şartla; temel bir-iki prensiple hareket etmeyi kabul etmelisiniz. Bunlar i. sitenize sizden başkası yazı yazmayacak ve ii. tüm yazılarınızı tek ve aynı PC'nizde yazıp, sonradan Gitlab'e yükleyeceksiniz. Yani, sizden başkası blog yazısı yazmayacağı gibi hep aynı bilgisayarınızı kullanarak yazı yazacaksınız ve yüklemeler tek yönlü (bilgisayardan Gitlab'e) olacak.

Bu prensipler dışında bir yöntem / uygulama düşünüyorsanız, lütfen ayrıca Git kullanımı üzerine ayrı bir araştırma yapın, çalışın. Aksi takdirde ezbere kullanmanız için paylaştığım 3 komut sizin düzeninizi bozacak ve veri kaybına da yol açacaktır.

Gelelim komutlara... Sitenizi klonladığınız **ana klasör yolunda** (veya ana dizinde) bir terminal penceresi açarsanız, aşağıdaki komutları kullanmaya başlayabilirsiniz. *Komutları burada paylaştığım sırada kullanmalısınız.*

1. `git add *`

Bu komutla eklediğiniz, kaldırdığınız veya taşıdığınız herhangi bir dosya varsa, Git'in envanterine dahil olur.

2. `git commit -am "açıklamanız"`

Bu komutla, açıklamanız kısmında belirttiğiniz değişiklikler dosyalara işlenir ve Gitlab'e yapılacak yükeleme sonrası adım adım işlem geçmişinizi log üzerinden takip edebilirsiniz.

3. `git push`

Herşey hazır olduğuna göre dosyalarınızı Gitlab reponuza yükleyebilirsiniz. Sizden kullanıcı adı ve şifre soracak olan Git, akabinde yerel PC'nizdeki bilgileri bulutla senkronize edecektir. 

## Hata Kontrolü
Diyelim herşey yolunda ve `git push` komutunu girip kullanıcı adı ve şifrenizi girdikten sonra senkronizasyon kendiliğinden gerçekleşti. Ancak yaptığınız düzenlemeler, Gitlab'in size yarattığı Hugo ortamında herhangi bir uyumsuzluğa / hataya neden oluyor mu olmuyor kontrol etmek istiyorsunuz (sitenin adresini girip çalışıp çalışmadığını kontrol etmek elbet bir yöntem :blush:)

Bunun için Gitlab sayfanıza gidip, son yüklemenizin durumunu sayfanın en üst hizasındaki 'yeşil check' işaretiyle kontrol edebilirsiniz. 

![Gitlab Pipeline Check](/yunbere/images/gitlab_pipeline_check.png#center) 

Burada 'yeşil check' yerine 'kırmızı X' varsa, kontrol etmeniz gereken bir sorun var demek. 'Kırmızı X'i tıklayarak açılan sayfaları takip ederseniz, Gitlab sizi sorunun olduğu loga götürecektir.   

# Tema Yüklemek
Hugo altında çalışabilen binlerce tema var. Bunlara [https://themes.gohugo.io/](https://themes.gohugo.io/) adresinden ulaşabilirsiniz. İstediğinizi siteniz için seçebilirsiniz.

Tema yüklemek son derece basittir. Paylaştığım adresten indireceğiniz temaları, yerel PC'nizde web sitenizin klasör yolunda `/themes/` klasörü altına kopyalamanız yeterlidir.

*Önerim, sitenizin ilk haliyle çalıştığını gördükten sonra tema yüklemeniz. İsterseniz şimdilik [Son Ayarlar](#son-ayarlar) başlığına geçip, sitenizin ilk kurulumunu tamamlayın.*

Temanızı uygun klasör altına koyduktan sonra yukarıdaki 3 Git komutunu sırayla kullanarak buluta yükleme yapabilirsiniz. Ancak, unutmayın ki temanızın aktif olması için bir sonraki bölümde tarif ettiğim `config.toml` dosyası içerisinde yer alan `theme` değişkeninin düzenlenmesi gerekir. Dolayısıyla, bu işlemi de yaptıktan sonra Gitlab'e yükleme yaparsanız işi tek seferde tamamlanmış olursunuz.

## Git ile Tema İndirmek 
Git kullanmak konusunda biraz pratik yaparsanız, ilgili temayı kaynağından bilgisayarınıza senkron olacak şekilde de yükleyebilirsiniz. 

*Hiç Git tecrübeniz yoksa, bu kısmı şimdilik atlayın. Sitenizi kurup çalıştırmayı başardıktan sonra isterseniz geri gelebilirsiniz.* 

Örneğin, bu sitenin kullandığı PaperMod temasını, sitenizin ana dizini altındayken aşağıdaki komutla yükleyebilirsiniz:
```
git clone https://github.com/adityatelange/hugo-PaperMod themes/PaperMod --depth=1
```

Bu sayede sonradan temayı güncellemek isterseniz, aşağıdakini yapmanız yeterli olur:
```
cd themes/PaperMod
git pull
```

`config.toml` dosyasındaki `theme` parametresini düzenledikten sonra, Gitlab'e ilgili komutlarla yükleme yapabilirsiniz.  

# Son Ayarlar
Hugo sitenizin tema içi ayarları haricindeki diğer tüm ayarları, ana dizininizdeki `config.toml` dosyasında bulur. 

PC'nizde bu dosyayı herhangi bir text editoru ile açabilirsiniz. İlk kullanımınızda, aşağıdaki değişiklikleri işlemeniz önemlidir:

1. `baseurl =` ile başlayan satırın sonuna web sitenizin adresini *gitlab.io* domaini ile ve çift tırnak içinde girmeniz gerekecek. 

Ör: `baseurl = "http://ged296123.gitlab.io/yunbere/"`

2. `theme =` ile başlayan satırın sonuna sitenizde kullandığınız temanın ismini girmeniz gerekecek. Bunun için tema sayfasını da lütfen inceleyin, orada daha açıkça belirtilmiştir. 

Ör: `theme = "PaperMod"`

*Eğer ilk kurulum aşamasındaysanız ve herhangi bir tema indirmediyseniz, bu değişkeni olduğu gibi bırakabilirsiniz.*

`config.toml` dosyasının diğer başlıklarını da incelerseniz, web sitenizin sayfaları, özeti, yazar bilgileri vb. bilgilerini aynı şekilde düzenleyebilirsiniz. 

Yerel PC'nizdeki `config.toml` düzenlemeleri tamamlandıktan sonra, yine yukarıda paylaştığım 3 Git komutunu sırasıyla uygulayabilirsiniz. 

# İlk Yazınızı Hazırlamak
Buraya kadar herhangi bir problemle karşılaşmadıysanız (ve hatta belki sitenizin istediğiniz temayla yayında olduğunu da görebiliyorsanız), yazı yazmaya başlayabiliriz! :blush:

Gitlab'den klonlamış olduğunuz klasörün `/content/post/` yolu altında örnek blog postlarını bulabilirsiniz. Bunları açıp bakarak genel yazı düzenlemesi vb. bilgileri öğrenebilirsiniz. Örneğin, her yazının/post'un başında bir künye olduğunu göreceksiniz.    

Ben postlarımın başında şimdilik aşağıdaki künyeyi kullanıyorum:

```
---
title: Bu Blog Gibi Bir Site de Siz Kurmak İster misiniz?
date: 2022-01-01
tags: ["atölye","izleniyoruz"]
draft: false
showtoc: true
---
```

Her yazının başında künye kullanmanız şart. Önerim, bu sitenin postlarını da Gitlab'den indirip bakın; ne ne işe yarıyor ordan da fayda sağlayabilirsiniz. 

Yazının başında bahsetmiştim, yazılarınızı herhangi bir text editor ile yazabilirsiniz. Yazılarınızın biraz süslü (veya biçimlendirilmiş) olmasını isterseniz, Markdown diye basit bir dilden de faydalanabilirsiniz. 

Markdown öğrenmek için internette sayısız site var. Birkaç örnek olması açısından aşağıdakileri paylaşıyorum:

[https://commonmark.org/help/](https://commonmark.org/help/)

[https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/)

[https://www.markdowntutorial.com/](https://www.markdowntutorial.com/)

Yazınızı bilgisayarınızda yazıp hazırladıktan sonra her zamanki 3 Git komutuyla Gitlab'le senkron olup, siz de kendi blog sitenizi yürütebiliriz!

İyi günlerde kullanmanız dileğiyle!

# Araştırabilecekleriniz

Bu kılavuzu en temel ve basit işlemleri tariflemek için hazırladım. Amacım Hugo, Git veya site nasıl yaratılır öğretmek değil, sizi sosyal medyadan kurtarıp yazılarınızı paylaşabileceğiniz bir blog sitesine kavuşturmak. Ancak bir yandan bu işler çok da eğlenceli ve öğretici olabiliyor. Kılavuz size fayda sağladı ve blogunuz iyi / kötü çalışıyorsa, kendinizi ve sitenizi aşağıdakileri araştırarak daha da geliştrebilirsiniz. 

- Yazılarınıza etiketler atayabilirsiniz. 
- Seçeceğiniz tema dökümanlarını okuyun, sitenize kullanışlı özellikler getirebilirler (sitede arama yapmak gibi).
- Yazılarınıza görseller ve ekler getirmenin yollarını araştırabilirisniz. 
- `config.toml` dosyasındaki ayarları araştırabilirsiniz. 
- Sitenizi kendi alan adınıza yönlendirebilirsiniz, ikonunuzu (favicon) değiştirebilirsiniz.
- Git çalışabilirsiniz. 
- Markdown kullanımınızı geliştirebilirsiniz. 
- Hugo'yu yerel makinanıza nasıl kuracağınızı öğrenebilirsiniz. Bu sayede belki bir gün kendi sitenizi kendiniz host edersiniz.

# Referans Siteler
Paylaştıklarımın çoğunu aşağıdaki 2 siteden elde ettim. Benim ifadem size iyi gelmiyor veya anlaşılmayan bir konu olduğunu düşünüyorsanız, bu sitelere siz de bakabilirsiniz:

https://kalikiana.gitlab.io/post/2021-02-12-setup-gitlab-pages-blog-with-hugo/ 

https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
