---
title: Bilgi
comments: false
---

Bu site, 100kb'nin altındaki büyüklüğüyle [512kbClub](https://512kb.club/)'ın gurulu bir *Yeşil Üyesi*dir.[^1] Interneti'i ve kullanıcılarını lüzümsuz büyüklükteki sitelerden korumayı desteklemektedir.

[^1]: Yazılara içerik olan fotograf, dosya ve benzeri eklentiler dahil değildir. 

Bu site, [Hugo](https://gohugo.io/) kullanılarak ve [PaperMod](https://github.com/adityatelange/hugo-PaperMod/) temasıyla [MIT](https://gitlab.com/Ged296123/yunbere/-/blob/master/LICENSE) lisansı altında **özgür yazılım** ilkeleriyle oluşturulmuş, [Gitlab](https://gitlab.com) sunucuları üzerinden yayınlanmaktadır. Kaynak koduna [buradan](https://gitlab.com/Ged296123/yunbere) ulaşabilirsiniz.
 
Sitenin burada belirtilen "yazılımlar" haricindeki her türlü içeriği, eser sahibi "Ged" tarafından oluşturulmuş ve [Creative Commons Atıf-AynıLisanslaPaylaş 4.0 Uluslararası Lisansı](http://creativecommons.org/licenses/by-sa/4.0/) ile lisanlandırılmıştır.

Bu siteyi oluşturan "Ged" sizden hiçbir veri toplamamaktadır.
